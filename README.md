# Computer vision
This repository will store all of my scripts and notebooks which are in some way
related to computer vision.

At the time of writing this, I had a strong(ish) background programming with
Python (having worked for 2 years as a software engineer at Codethink) but very
little experience using MatplotLib and zero experience with OpenCV.

Computer vision is something which (for now...) fascinates me and this repo will
hopefully be a manifestation of my progression in the area.

## Converting an image to a sketch in Python
The notebook
[convert-image-to-sketch.ipynb](notebooks/convert-image-to-sketch.ipynb) reads
in an image using OpenCV, makes it grayscale, obtains the negative, applies a
Gaussian blur, blends the image with a dodging function and then plots this on
top of an image of a canvas so that it looks like a sketch.

I faced many problems here, firstly a complete lack of understanding to the
Gaussian blur and the dodging and burning techniques and secondly, trying to get
the two matrices of the canvas and the blended image the same size. Through
inspection, I noticed that the transpose of the canvas image (well, the matrix
representing it) was the same size as the blended image. I tried this for a few
more images and it seemed to keep working.

Below is an example of the script which reads in the following [pinwheel](images/pinwheel.jpg) image:

![alt text](images/pinwheel.jpg "Pinwheel")

And produces the following sketch:

![alt text](images/results_pinwheel_sketch.png "Pinwheel sketch")


## Edge detection in Python
### Why bother?
The foundation of object recognition in software begins at the ability to be
able to detect edges in a given image.

An edges only image is just black and white, so naturally we get rid of most of
the detail, therefore a lot less expensive. Therefore, important in cases where
we don't need to care about the detail, just the shape.

The purpose of detecting edges is to capture important events and changes in
properties of the world. It is one of the fundamental steps in image processing,
image pattern recognition and computer vision techniques.

Now, as humans we're good at identifying edges, but how do we teach our computers
to do this for us?

### My work
The script [edges.py](scripts/edges.py) reads in an image and then finds the
edges of that image. It does this in two ways, both using the Canny algorithm
but one being a very crude version where we loop over each pixel and compare it
to the surrounding 8 pixels to determine whether its an edge (see the theory
[here](https://towardsdatascience.com/edge-detection-in-python-a3c263a13e03)),
and the other using OpenCV's Canny function, which does a lot more, see
[here](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html).

Both methods record the time time and the result produces three images, the
original, the image produced by the crude algorithm and the image produced by
OpenCV's canny algorithm.

Below is the result of the code when using the pinwheel image:

![alt text](images/results_edges.png "Pinwheel edges")

The first script made it clear that OpenCV's version is a lot more efficient, as
well as produces a better result. So, I then I used this to record edges real
time from the webcam. This is achieved in the script
[videocapture.py](scripts/videocapture.py).

This can simply be run with `python3 videocapture.py`, ensure you have OpenCV
and numpy installed.

Finally, I wanted some MatplotLib practice and also I didn't really understand
the effect of the two thresholds used in CV2's Canny algorithm, so I implemented
a script which uses sliders to allow you to change the value of the two
thresholds and see the effect on the image. This is in the script
[canny_with_slider.py](scripts/canny_with_slider.py)

Below are two examples of the pinwheel image with initial and then changed
threshold values.

Initial values:

![alt text](images/results_edges_default_thresholds.png "Default thresholds")

Changed values:

![alt text](images/results_edges_changed_thresholds.png "Changed thresholds")

## Finding and drawing contours
Contours are the lines which join all points alone a boundary (or edge) of the
same intensity. Essential for object detection and finding the size of an object
of interest. OpenCV provides _findContour_ and _drawContours_ which does all of
the heavy lifting for us.

The [contours.py](scripts/contours.py) script finds and draws the contours of
the following image:

![alt text](images/results_contours.png "Finding contours")
