import os

import imutils
import numpy as np
import cv2

# Prelude
#
# This script uses OpenCV's dnn module function blobFromImage(). This preprocesses
# an image and prepares it for classification (when we pass it into a network)
# Part of the preprocessing is mean subtraction, this helps to combat illumination
# changes in the image. To do this, we calculate the average pixel intensity
# across the images in the training set for each R,G,B channel and then for each
# and every pixel in the image(s) we take the pixel R,G,B values and subtract the
# respective mean values
# Sometimes we divide each result by a scaling factor.
# Not all deep learning architectures perform mean subtraction, or scaling, or both.


# Load the ImageNet class labels
rows = open("synset_words.txt").read().strip().split("\n")
# Extract the class names from the file
classes = [r[r.find(" ") + 1:].split(",")[0] for r in rows]

# Load our serialized model
net = cv2.dnn.readNetFromCaffe("bvlc_googlenet.prototxt",     # Filename
	                           "bvlc_googlenet.caffemodel")   # Model file

imagePaths = [os.path.join("images", f) for f in os.listdir("images")]

# (1) load the first image from disk
# (2) pre-process it by resizing it to 224x224 pixels, the required size for GoogLeNet
# (3) construct a blob that can be passed through the pre-trained network
image = cv2.imread(imagePaths[0])
resized = cv2.resize(image, (224, 224))

# blobFromImage()
#
# resized is the input image we want to preprocess
# 1, is the scale factor after we perform mean subtraction
# (244,244) is the spatial size that the Convolutional neural network expects
# 104, 117 and 123 are our mean R,G,B channels
# By default, this function will take care of our BGR image and swap it to RGB
# returns a blob  which is our input image after mean subtraction, normalizing,
# and channel swapping
blob = cv2.dnn.blobFromImage(resized, 1, (224, 224), (104, 117, 123))
print("First Blob: {}".format(blob.shape))

# Set the input to the pre-trained deep learning network and obtain
# the output predicted probabilities for each of the 1,000 ImageNet
# classes
net.setInput(blob)
preds = net.forward()

# Sort the probabilities (in descending) order, grab the index of the
# top predicted label, and draw it on the input image
# preds[0] is the prediction value of the first image against every class
descending = np.argsort(preds[0])[::-1]
idx = descending[0]
text = "Label: {}, {:.2f}%".format(classes[idx],
	preds[0][idx] * 100)
cv2.putText(image, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,
	        0.7, (0, 0, 255), 2)

# Show the output image with the inputted text
cv2.imshow("Image", image)
cv2.waitKey(0)

#####################################
# Demonstration of blobFromImages() #
#####################################
# Initialise the list of images we'll be passing through the network
images = []

# loop over the input images (excluding the first one),
# pre-process each image, and update the `images` list
for p in imagePaths[1:]:
	image = cv2.imread(p)
	image = cv2.resize(image, (224, 224))
	images.append(image)

# convert the images list into an OpenCV-compatible blob
blob = cv2.dnn.blobFromImages(images, 1, (224, 224), (104, 117, 123))
print("Second Blob: {}".format(blob.shape))

# set the input to our pre-trained network and obtain the output class label predictions
net.setInput(blob)
preds = net.forward()

# loop over the input images
for (i, p) in enumerate(imagePaths[1:]):
	# load the image from disk
    image = cv2.imread(p)
    image = imutils.resize(image, width=600)  # Make them all a sensible size

	# Find the top class label prediction and draw it on
    descending = np.argsort(preds[i])[::-1]
    idx = descending[0]
    text = "Label: {}, {:.2f}%".format(classes[idx],              # Name of class
		                               preds[i][idx] * 100)       # Confidence of prediction
    cv2.putText(image, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,
                0.7, (0, 0, 255), 2)

    # display the output image
    cv2.imshow("Image", image)
    cv2.waitKey(0)
