# Image basics/manipulation with the openCV library

import cv2
import imutils
from matplotlib import pyplot as plt

import sys

# Load the input image and show its dimensions
#   - Images are represented as a multi-dimensional NumPy array with
#     shape: rows (height) x columns (width) x channels (depth)
image = cv2.imread("jp.png")
(h, w, d) = image.shape
print("width={}, height={}, depth={}".format(w, h, d))


# Display the image to our screen using OpenCV
cv2.imshow("Image", image)
cv2.waitKey(0)

# Display the image to our screen using matplotlib
# Note that opencv uses BGR (meaning that the arrays representing the images
# are backwards), matplotlib expects RGB, therefore we need to convert.
plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
plt.show()

# Access the RGB pixel located at x=50, y=100, keepind in mind that
# OpenCV stores images in BGR order rather than RGB
(B, G, R) = image[100, 50]
print("R={}, G={}, B={}".format(R, G, B))

# Extract a 100x100 pixel square ROI (Region of Interest) from the
# input image starting at x=320,y=60 at ending at x=420,y=160
roi = image[60:160, 320:420]
cv2.imshow("ROI", roi)
cv2.waitKey(0)

# Resize the image to 200x200px, ignoring aspect ratio
# Funfact: In the case of deep learning, images are often resized ignoring
# aspect ratio, so that the volume fits into a network which requires that
# an image be square and of a certain dimension.
resized = cv2.resize(image, (200, 200))
cv2.imshow(f"Resized to 200, 200 from {w}, {h}", resized)
cv2.waitKey(0)

# Fixed resizing and distort aspect ratio so let's resize the width
# to be 300px but compute the new height based on the aspect ratio
r = 300.0 / w  # ratio of new width to old width
dims = (300, int(h * r))
resized = cv2.resize(image, dims)
cv2.imshow("Aspect Ratio Resize", resized)
cv2.waitKey(0)

# Manually computing the aspect ratio can be a pain so let's use the
# imutils library instead
resized = imutils.resize(image, width=1000)
cv2.imshow("Imutils Resize", resized)
cv2.waitKey(0)

# Rotate the image 45 degrees clockwise
#  - First compute the center
#  - Construct rotation matrix
#  - Finally apply xaffine warp
center = (w // 2, h // 2)  # // is floor division (dumps the digits after the decimal)
M = cv2.getRotationMatrix2D(center, -45, 1.0)  # third arg is scale
rotated = cv2.warpAffine(image, M, (w, h))
cv2.imshow("OpenCV Rotation", rotated)
cv2.waitKey(0)

# Rotate using imutils
rotated = imutils.rotate(image, 45)
cv2.imshow("Imutils Rotation", rotated)
cv2.waitKey(0)

# OpenCV doesn't "care" if our rotated image is clipped after rotation
# so we can instead use another imutils convenience function to help
# us out
rotated = imutils.rotate_bound(image, 45)
cv2.imshow("Imutils Bound Rotation", rotated)
cv2.waitKey(0)

# Apply Gaussian blur with a 11x11 kernel to smooth image,
#  - Useful when reducing high frequency noise
#  - Larger kernels yield more blurry images
blurred = cv2.GaussianBlur(image, (11, 11), 0)
cv2.imshow("Blurred", blurred)
cv2.waitKey(0)

# Draw a 2px thick red rectangle surrounding the face
output = image.copy()
# cv2.rectangle(image, start_point, end_point, color, thickness)
cv2.rectangle(output, (300, 20), (430, 160), (0, 0, 255), 2)
cv2.imshow("Rectangle", output)
cv2.waitKey(0)

# Draw a blue 20px (filled in) circle on the image centered at
# x=300,y=150
output = image.copy()
# Last arg is thickness, -1 will fill shape by specified color
cv2.circle(output, (300, 150), 20, (255, 0, 0), -1)
cv2.imshow("Circle", output)
cv2.waitKey(0)

# Draw a 5px thick red line from x=60,y=20 to x=400,y=200
output = image.copy()
cv2.line(output, (60, 20), (400, 200), (0, 0, 255), 5)
cv2.imshow("Line", output)
cv2.waitKey(0)

# Draw green text on the image
output = image.copy()
cv2.putText(output, "OpenCV + Jurassic Park!!!", (10, 25), 
	cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
cv2.imshow("Text", output)
cv2.waitKey(0)
