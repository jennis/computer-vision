import argparse

import imutils
import cv2

import sys

# Construct the argument parser and parse the args
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())

# Load and display the image
image = cv2.imread(args["image"])
cv2.imshow("Image", image)
cv2.waitKey(0)

# Convert to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # Enum equivalent to 0
cv2.imshow("Grayscaled", gray)
cv2.waitKey(0)

# Apply Canny edge detection to find object outlines
edged = cv2.Canny(gray, 30, 150)
cv2.imshow("Edged", edged)
cv2.waitKey(0)

# Threshold the image by setting all (grayscaled) pixel values less than 225
# to 0 (white; foreground) and all pixel values >= 225 to 255
# (black; background), thereby segmenting the image
# Segmenting foreground from background with a binary image is critical to finding contours
thresh = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)[1]
cv2.imshow("Threshold", thresh)
cv2.waitKey(0)

# Find the contours of the foreground objects in the thresholded image
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                        cv2.CHAIN_APPROX_SIMPLE)
# I guess this just takes the contour list out of the tuple returned by cv2.findContours()
cnts = imutils.grab_contours(cnts)

# Loop over the contours
output = image.copy()
for c in cnts:
	# Draw each contour on the output image with a 3px thick purple
	# outline, then display the output contours one at a time
	cv2.drawContours(output, [c], -1, (240, 0, 159), 3)
	cv2.imshow("Contours", output)
	cv2.waitKey(0)

# Add text for the number of contours found
text = "{} objects found!".format(len(cnts))
cv2.putText(output, text, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.7,
	(0, 0, 255), 2)
cv2.imshow("Contours", output)
cv2.waitKey(0)

# Erosions and dilations are often used to reduce noise in binary images

# We apply erosions to reduce the size of foreground objects (erode away pixels)
mask = thresh.copy()
mask = cv2.erode(mask, None, iterations=5)
cv2.imshow("Eroded", mask)
cv2.waitKey(0)

# Similarly, dilations can increase the size of the foreground objects
mask = thresh.copy()
mask = cv2.dilate(mask, None, iterations=5)
cv2.imshow("Dilated", mask)
cv2.waitKey(0)

# Mask allows us to mask out regions we're not interested in
# We use the thresh image and mask it with the original image
# A typical operation we may want to apply is to take our mask and
# apply a bitwise AND to our input image, keeping only the masked
# regions
mask = thresh.copy()
output = cv2.bitwise_and(image, image, mask=mask)
cv2.imshow("Output", output)
cv2.waitKey(0)
