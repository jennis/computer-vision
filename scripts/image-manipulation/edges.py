import os
import sys
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt


f = "images/pinwheel.jpg"
if not os.path.exists(f):
    sys.exit(f"File {f} does not exist")

# read in the image
img = cv2.imread(f)

##############################################################
# Using raw code to analyse the surroundings of each pixel #
##############################################################
t0 = time.time() # time how long this process takes

# Create an empty array of the same size as the image we'll be populating this.
edges_raw = np.zeros_like(img)

# Define the vertical and horizontal filter matrices and then loop over every pixel
# in the image, take a 3x3 matrix of surrounding pixels and multiply by these
# matrices
vertical_filter = [[-1,-2,-1], [0,0,0], [1,2,1]]
horizontal_filter = [[-1,0,1], [-2,0,2], [-1,0,1]]

# get the dimensions of the image
n, m, _ = img.shape
total_pixels = n*m
print(f"Looping through {n}x{m}={total_pixels} pixels")
counter = 0
for row in range(3, n-2):
    for col in range(3, m-2):
        if counter % 100000 == 0:
            print(f"({round((counter/total_pixels)*100)}%) pixels complete")

        # create little local 3x3 box
        local_pixels = img[row-1:row+2, col-1:col+2, 0]

        # apply the vertical filter
        vertical_transformed_pixels = vertical_filter*local_pixels
        # remap the vertical score
        vertical_score = vertical_transformed_pixels.sum()/4

        # apply the horizontal filter
        horizontal_transformed_pixels = horizontal_filter*local_pixels
        # remap the horizontal score
        horizontal_score = horizontal_transformed_pixels.sum()/4

        # combine the horizontal and vertical scores into a total edge score
        # sqrt(a^2 + b^2)
        edge_score = (vertical_score**2 + horizontal_score**2)**.5

        # insert this edge score into the edges image
        edges_raw[row, col] = [edge_score]*3

        counter += 1

# remap the values in the 0-1 range in case they went out of bounds
edges_raw = edges_raw/edges_raw.max()

# Timings
t1 = time.time()
raw_time = t1 - t0

###########################################
# Using the canny edge detector algorithm #
###########################################
t0 = time.time()

edges_canny = cv2.Canny(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), threshold1=30, threshold2=100)

t1 = time.time()
canny_time = t1 - t0

########################
# Plotting the results #
########################
fig=plt.figure(figsize=(8, 8))

fig.add_subplot(1, 3, 1)
plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))  # OpenCV represents images a BGR
plt.title("Original image")
plt.axis("off")

fig.add_subplot(1, 3, 2)
plt.imshow(edges_raw)
plt.title(f"Raw code\nTime: {round(raw_time,3)}s")
plt.axis("off")

fig.add_subplot(1, 3, 3)
plt.imshow(edges_canny)
plt.title(f"Canny algorithm\nTime: {round(canny_time,3)}s")
plt.axis("off")


plt.show()
