import os
import sys

import cv2
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider

f = "images/pinwheel.jpg"
if not os.path.exists(f):
    sys.exit(f"File {f} does not exist")

# read in the image
img = cv2.imread(f)

# Obtain the original image that we'd like to put
t1 = 30
t2 = 100
edges_img = cv2.Canny(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
                        threshold1=t1,
                        threshold2=t2)

########
# Plot #
########
fig = plt.figure(figsize=(8,3))
img_ax = plt.axes([0.1, 0.2, 0.8, 0.65])
t1_slider_ax = plt.axes([0.25, 0.15, 0.65, 0.03])
t2_slider_ax = plt.axes([0.25, 0.1, 0.65, 0.03])

plt.axes(img_ax) # select the img ax
plt.title(f"Canny algorithm")
plt.axis('off')
plt.imshow(edges_img)

# Create the sliders for thresholds 1 and 2
t1_slider = Slider(t1_slider_ax,      # the axes object containing the slider
                   'Threshold1',      # the name of the slider parameter
                   1,                 # minimal value of the parameter
                   100,               # maximal value of the parameter
                   valinit=t1)        # initial value of the parameter

t2_slider = Slider(t2_slider_ax, 'Threshold2', 1, 100, valinit=t2)

# Update function that will be executed each time a slider value changes.
def update(val):
    t1_selected = t1_slider.val
    t2_selected = t2_slider.val
    plt.imshow(cv2.Canny(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
                         threshold1=t1_selected,
                         threshold2=t2_selected))
    # redraw the plot
    fig.canvas.draw_idle()

# Ensure the sliders update when a value changes
t1_slider.on_changed(update)
t2_slider.on_changed(update)

plt.show()
