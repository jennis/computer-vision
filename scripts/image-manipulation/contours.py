# Use OpenCV to detect and draw contours
import os
import sys

import cv2
import numpy as np
from matplotlib import pyplot as plt

f = "images/shapes.png"
if not os.path.exists(f):
    sys.exit(f"File {f} does not exist.\nExiting...")

img = cv2.imread(f)
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Find the edges
edges = cv2.Canny(img_gray, 30, 100)

# Find the contours Contours is a python list of all contours in the img
# Each contour is a array of (x,y) coordinates of the boundary. The contour
# approximation method stores appropriate (x,y) coords of the boundary. CHAIN
# APPROX NONE stores all boundary points. CHAIN APPROX SIMPLE stores endpoints
contours, hierarchy = cv2.findContours(edges,
                                       cv2.RETR_EXTERNAL,      # Contour retrieval mode
                                       cv2.CHAIN_APPROX_SIMPLE)  # Contour approximation method
#contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)  # This finds a lot more contours...

print(f"Number of Contours found: {len(contours)}")

# Draw all contours
# -1 signifies drawing all contours
cv2.drawContours(img, contours, -1, (0, 255, 0), 3)


###############
# Plot images #
###############
fig=plt.figure(figsize=(8, 8))

plt.subplot(2,1,1)
plt.imshow(edges)
plt.axis('off')
plt.title("Canny edges after finding contours")

plt.subplot(2,1,2)
plt.imshow(img)
plt.axis('off')
plt.title(f"Contours drawn: {len(contours)}")

plt.show()
