import time

import click
import cv2
import imutils
import numpy as np
from imutils.video import VideoStream


# construct the argument parse and parse the arguments
@click.command()
@click.option('-p', '--prototxt',type=click.Path(exists=True), required=True)
@click.option('-m', '--model', type=click.Path(exists=True), required=True)
@click.option('-c', '--confidence', type=float, default=0.5,
              help='Minimum probability to filter weak detections')
def main(prototxt, model, confidence):
    # load our serialized model from disk
    print("[INFO] loading model...")
    net = cv2.dnn.readNetFromCaffe(prototxt, model)

    # Initialise the video stream and allow the camera sensor time to warmup
    print("[INFO] starting video stream...")
    vs = VideoStream(src=0).start()  # Camera with index 0 is generally the in built camera
    time.sleep(2.0)

    # loop over the frames from the video stream and apply face detection
    while True:
        # grab the frame from the threaded video stream and resize it
        # to have a maximum width of 400 pixels
        frame = vs.read()
        frame = imutils.resize(frame, width=400)

        # grab the frame dimensions and convert it to a blob
        h, w, _ = frame.shape
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
            (300, 300), (104.0, 177.0, 123.0))

        # pass the blob through the network and obtain the detections and
        # predictions
        net.setInput(blob)
        detections = net.forward()

        # loop over the detections
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence_val = detections[0, 0, i, 2]

            # filter out weak detections
            if confidence_val < confidence:
                continue

            # compute the (x, y)-coordinates of the bounding box
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # draw the bounding box of the face along with the associated
            # probability
            cv2.rectangle(frame, (startX, startY), (endX, endY),
                          (0, 0, 255), 2)

            text = "{:.2f}%".format(confidence_val * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.putText(frame, text, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

        # Show the output frame
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        # If the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    # do a bit of cleanup
    cv2.destroyAllWindows()
    vs.stop()

if __name__ == '__main__':
    main()
