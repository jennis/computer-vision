import logging

import click
import cv2
import numpy as np

logger = logging.getLogger('detect_faces')
logger.setLevel(logging.INFO)

@click.command()
@click.argument('image', type=click.Path(exists=True))
@click.option('-p', '--prototxt',type=click.Path(exists=True), required=True)
@click.option('-m', '--model', type=click.Path(exists=True), required=True)
@click.option('-c', '--confidence', type=float, default=0.5,
              help='Minimum probability to filter weak detections')
def main(image, prototxt, model, confidence):
    """Simple program to detect faces in an image"""

    # Load the serialized model
    logger.info("Loading prototxt and model...")
    net = cv2.dnn.readNetFromCaffe(prototxt, model)

    # Load the input image and construct an input blob for the image
    # by resizing to a fixed 300x300 pixels and then normalizing it
    image = cv2.imread(image)
    # Remember this is a numpy array of rows x columns (so height first)
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    ##################
    # Face detection #
    ##################
    # Pass the blob through the network and obtain the detections and
    # predictions
    logger.info('Computing object detections...')
    net.setInput(blob)
    detections = net.forward()

    # Loop over the detections and do shit
    for i in range(0, detections.shape[2]):
        # Extract the confidence (i.e., probability) associated with the
        # prediction
        confidence_val = detections[0, 0, i, 2]

        # Filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence_val > confidence:
            # compute the (x, y)-coordinates of the bounding box for the
            # object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # Draw the bounding box of the face along with the associated
            # probability
            cv2.rectangle(image, (startX, startY), (endX, endY),
                (0, 0, 255), 2)

            # Put the text top left of box if possible (or slightly below if not)
            text = "{:.2f}%".format(confidence_val * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.putText(image, text, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

    # Show the output image
    cv2.imshow("Output", image)
    cv2.waitKey(0)

if __name__ == '__main__':
        main()
